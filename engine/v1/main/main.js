const puppeteer = require('puppeteer');
const path      = require('path');

const scenario_file = process.argv[2];
const main_conf = require( path.resolve(scenario_file) );
const libraries = require(  '../../../libraries/libraries' );

(async () => {

  // Loop Platform or Website
  for (const platform_conf of main_conf) {

    // initiate framework
    const browser = await puppeteer.launch( platform_conf.config || {
      headless: false,
      devtools: false,
      defaultViewport: null,
      slowMo: 90,
      args:[
          "--start-maximized"
      ],
    });


    try {
      const page = await browser.newPage();

      // Load Host browser
      await page.goto(platform_conf.host || "http://example", {
        timeout: 0,
        waitUntil: platform_conf.waitUntil  || "networkidle0",
      });

      // Load the scenes
      console.log(">> start");
      for (const object_scene of platform_conf.scenes) {

        // print name
        console.log('>>> '+ object_scene.name );

        // find module
        const moduleLoaded = require(  path.resolve(object_scene.scene));
        // setup to the scene
        await moduleLoaded(page, object_scene.configuration, libraries); 
    
      }
      console.log(">> end");

      if ( platform_conf.close ) {
        await browser.close();
      }

    } catch (error) {
      console.log(error);

      if ( platform_conf.close ) {

        await browser.close();

      }

    }

  }

})();
