
module.exports = (async (page, params, libraries) => {

    params = params || {};

    const search = params.search  || "turtleio";

    // type something to element
    await libraries.input(page,'input[name=q]', search);

    // enter
    await libraries.enter(page);


    // Screenshot last exploring
    await libraries.screenshot(page, 'searching_with_google');
})