
module.exports = (async (page, params, libraries) => {

    params = params || {};

    const search = params.search  || "xx";

    // clicking element
    await libraries.click(page, 'selector');

    // text something
    await page.waitForSelector( 'waiting for text selector' );

    // type something to element
    await libraries.input(page,'input search institution selector', search);

    // upload something in button
    const clickElementUpload = 'input[type=file] selector';
    await page.waitForSelector( clickElementUpload );

    // select Element Something
    const selectElement = 'selector select';
    await page.waitForSelector( selectElement );
    await page.select( selectElement, "FIND_VALUE" ); // Jawa Barat
    

    // Screenshot last exploring
    await libraries.screenshot(page, 'nama_file_or_something');
})