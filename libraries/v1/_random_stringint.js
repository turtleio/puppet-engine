
module.exports = ((length, prefix) => {

    if (typeof length === "undefined") {
      length = 5;
    }
    if (typeof prefix === "undefined") {
      prefix = ""
    }
  
    var result           = prefix;
    var characters       = '0123456789';
    if (length === 1) {
      characters = '123456789';
    }
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
});