
module.exports = (async (page, element_name, input_value) => {
    // Typing Element
    await page.waitForSelector(element_name);
    await page.evaluate( (element_name) => { document.querySelector( element_name ).value = ""; }, element_name);
    await page.type(element_name, input_value); 
});