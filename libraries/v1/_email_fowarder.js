
module.exports = (( email ) => {

    const list_of_part  = email.split("@");
    const date_now      = new Date();

    const result        = [ list_of_part[0],'+', date_now.getTime(), '@', list_of_part[1] ].join('');

    return result;
});