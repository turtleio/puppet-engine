
module.exports = (async (page, element_name, input_value) => {
    // Typing Element
    await page.waitForSelector(element_name);
    await page.select(element_name, "" + input_value ); 
    await page.waitForTimeout( 4000 );
});
