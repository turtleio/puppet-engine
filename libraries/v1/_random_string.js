
module.exports = ((length, prefix) => {

    var result           = '';
    
    if (typeof length === "undefined") {
        length = 5;
    }
    if (typeof prefix !== "undefined") {
        result = prefix;
    }

    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
});