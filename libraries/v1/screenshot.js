
module.exports = (async (page, name) => {
    // Screenshot last exploring
    const date_take = new Date();
    await page.screenshot({ path: [ 'report/screenshot/', date_take.getTime(), '_',name,'.png'].join('') });
    
});