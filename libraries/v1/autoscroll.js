
module.exports = (async (page, distance_repeater, lengthHeight) => {

    await page.evaluate(async (distance_repeater, lengthHeight) => {

        await new Promise((resolve, reject) => {
            if (typeof lengthHeight !== 'undefined') {
              var totalHeight = lengthHeight;
            }
            var distance = distance_repeater || 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;
  
                if(totalHeight <= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 1000);
        });

    }, distance_repeater, lengthHeight);
});