
module.exports = (( age ) => {
    age = typeof age === 'undefined' ? 0 : age;

    var d = new Date();

    d.setFullYear( d.getFullYear() - age );

    var month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day  ].join('-');

});