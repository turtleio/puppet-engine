
module.exports = (async (page, element_name) => {
    // Clicking Element
    await page.waitForSelector(element_name);
    await page.click(element_name); 
});