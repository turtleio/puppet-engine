// utilities
exports._random_string      = require('./v1/_random_string');
exports._random_stringint   = require('./v1/_random_stringint');
exports._email_fowarder     = require('./v1/_email_fowarder');
exports._date_from_age     = require('./v1/_date_from_age');

// shortcut function
exports.screenshot      = require('./v1/screenshot');
exports.click           = require('./v1/click');
exports.input           = require('./v1/input');
exports.select          = require('./v1/select');
exports.enter          = require('./v1/enter');
exports.autoscroll      = require('./v1/autoscroll');
